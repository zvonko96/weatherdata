import { Injectable } from '@angular/core';
import { IWheater } from './models/DataWeather';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor() { }

  wheaterData: IWheater = {
    tempUnit: 'C',
    windSpeedUnit: 'm/s',
    days: [
      { day: 'Mon', temp: 22, windDirection: 'north-east', windSpeed: 10, type: 'sun-o' },
      { day: 'Tue', temp: 14, windDirection: 'north-west', windSpeed: 14, type: 'cloud' },
      { day: 'Wed', temp: 13, windDirection: 'south-east', windSpeed: 20, type: 'snowflake-o' },
      { day: 'Thu', temp: 15, windDirection: 'south-east', windSpeed: 25, type: 'cloud' },
      { day: 'Fri', temp: 16, windDirection: 'south-east', windSpeed: 13, type: 'snowflake-o' },
      { day: 'Sat', temp: 19, windDirection: 'north-east', windSpeed: 15, type: 'sun-o' },
      { day: 'Sun', temp: 16, windDirection: 'south-east', windSpeed: 17, type: 'cloud' },
      { day: 'Mon', temp: 15, windDirection: 'south-east', windSpeed: 19, type: 'snowflake-o' },
      { day: 'Tue', temp: 14, windDirection: 'south-east', windSpeed: 27, type: 'sun-o' },
      { day: 'Wed', temp: 10, windDirection: 'north-west', windSpeed: 16, type: 'snowflake-o' },
      { day: 'Thu', temp: 14, windDirection: 'north-east', windSpeed: 27, type: 'sun-o' },
      { day: 'Fri', temp: 10, windDirection: 'south-east', windSpeed: 16, type: 'snowflake-o' }
    ]
  }



  public getWheaterData() {
    return this.wheaterData;
  }
  
}
