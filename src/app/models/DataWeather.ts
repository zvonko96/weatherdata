
export class IDays {
    day: string;
    temp: number;
    windDirection: string;
    windSpeed: number;
    type: string;
}

export class IWheater {
    tempUnit: string;
    windSpeedUnit: string;
    days: Array<IDays>;
}