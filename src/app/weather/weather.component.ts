import { IDays } from './../models/DataWeather';
import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../weather.service';
import { IWheater } from '../models/DataWeather';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  wheaterData: IWheater;
  
  currentDayData: IDays;
  windDirection: number; 
  page: number = 1;

  constructor(private dataService: WeatherService) {

   }

  ngOnInit() {
    this.wheaterData = this.dataService.getWheaterData();
    this.currentDayData = this.wheaterData.days[0];
    this.checkWindOrinentation(this.currentDayData)
  }

  // setting the current day on clikc
  setCurrentDay(day) {
    this.checkWindOrinentation(day)
    this.currentDayData = day;
  }

  // setting rotatio of wind arrow
  checkWindOrinentation(day) {
    switch (day.windDirection) {
      case 'north-east':
        this.windDirection = 180;
        break;
      case 'south-east':
        this.windDirection = 90;
        break;
      case 'nort-west':
        this.windDirection = 135;
        break;
      case 'south-west':
        this.windDirection = 275;
        break;
      case 'south':
        this.windDirection = 270;
      case 'north':
        this.windDirection = 90;
      case 'east':
        this.windDirection = 0;
      case 'west':
        this.windDirection = 180;
    }
  }

  // conversion of given type
  public convert(data) {
    if (data == 'temp') {
      if (this.wheaterData.tempUnit == 'C') { 
        for (let i = 0; i < this.wheaterData.days.length; i ++) {
          this.wheaterData.days[i].temp = this.wheaterData.days[i].temp + 273.15
        }
        this.wheaterData.tempUnit = 'K'
      } else {
        for (let i = 0; i < this.wheaterData.days.length; i ++) {
          this.wheaterData.days[i].temp = this.wheaterData.days[i].temp - 273.15
        }
        this.wheaterData.tempUnit = 'C'
      }
    } else if(data == 'speed') {
      if (this.wheaterData.windSpeedUnit == 'm/s') {
        for (let i = 0; i < this.wheaterData.days.length; i ++) {
          this.wheaterData.days[i].windSpeed = this.wheaterData.days[i].windSpeed * 3.6
        }
        this.wheaterData.windSpeedUnit = 'km/h'
      } else {
        for (let i = 0; i < this.wheaterData.days.length; i ++) {
          this.wheaterData.days[i].windSpeed = this.wheaterData.days[i].windSpeed / 3.6
        }
        this.wheaterData.windSpeedUnit = 'm/s'
      }
    }
  }

  

}



